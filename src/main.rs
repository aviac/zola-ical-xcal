use std::ffi::OsStr;
use std::str::FromStr;
use std::{env::args, path::PathBuf};

use anyhow::anyhow;
use itertools::Itertools;
use serde::Deserialize;

const OPTIMAL_LINE_WIDTH: usize = 75;

fn main() -> anyhow::Result<()> {
    // WONTFIX: not going to use clap for this small app
    if args().len() != 2 {
        anyhow::bail!("Just use one argument");
    }

    // recursively walks through all the files in the directory, finds all markdown files with toml
    // headers, and parses the headers into [`CalendarEntry`]s
    let tables = args()
        .nth(1)
        .ok_or(anyhow!("first argument is a directory name"))
        .and_then(|arg| PathBuf::from_str(arg.as_str()).map_err(anyhow::Error::from))
        .and_then(|path| {
            path.is_dir()
                .then_some(path)
                .ok_or(anyhow!("expected argument to be a directory"))
        })
        .map(|path| walkdir::WalkDir::new(path))?
        .into_iter()
        .filter_map(|path| path.ok())
        .map(|path| path.into_path())
        .filter(|path| path.is_file())
        .filter(|path| path.extension().is_some_and(|ext| ext == OsStr::new("md")))
        .filter_map(|path| {
            std::fs::read_to_string(path)
                .ok()
                .and_then(|content| content.split("+++").nth(1).map(|t| t.to_owned()))
                // calendar entries need at least a date, otherwise they aren't really useful
                .filter(|toml| toml.contains("date"))
                .and_then(|toml| toml::from_str::<CalendarEntry>(&toml).ok())
        });

    // use old calendar headers, copy-pasterino woop ✨
    let calendar = format!(
        "
BEGIN:VCALENDAR
VERSION:2.0
PRODID:-//C3D2-Web//event iCal-Export//DE
CALSCALE:GREGORIAN
METHOD:PUBLISH
X-WR-CALDESC;VALUE=TEXT:Events und Themenabende beim CCC Dresden
X-WR-CALNAME;VALUE=TEXT:C3D2-Events
X-WR-TIMEZONE;VALUE=TEXT:Europe/Prague

{entries}
END:VCALENDAR
",
        entries = tables.into_iter().map(|c| c.to_string()).join("\n")
    );

    println!("{}", calendar.trim());

    Ok(())
}

/// a lot of optional code. parsing this further mostly doesn't make sense. E.g. having dates in
/// e.g. chrono DateTimes doesn't provide any real value
#[derive(Deserialize)]
pub struct CalendarEntry {
    title: Option<String>,
    summary: Option<String>,
    description: Option<String>,
    categories: Option<Vec<String>>,
    duration: Option<String>,
    time: Option<String>,
    date: String,
    end: Option<String>,
    url: Option<String>,
    location: Option<String>,
}

impl std::fmt::Display for CalendarEntry {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        // not sure where to put the title so we just glue in front of the summary
        let summary = match (self.title.as_ref(), self.summary.as_ref()) {
            (None, None) => None,
            (None, summary) => summary.cloned(),
            (title, None) => title.cloned(),
            (Some(title), Some(summary)) => Some(format!("{title} - {summary}")),
        };
        // max allowed len is 255
        let summary = summary.map(|s| s.chars().take(255).collect::<String>());

        // smash date in front of time -> 🪄 -> DateTime ✨
        let datetime = self
            .time
            .as_ref()
            .map(|time| {
                // time has to have minutes and seconds
                // assume really stupid things 🤪 to ensure this
                match time.chars().filter(|&c| c == ':').count() {
                    0 => format!("{time}:00:00"),
                    1 => format!("{time}:00"),
                    _ => format!("{time}"),
                }
            })
            .map(|time| format!("{date}T{time}", date = self.date))
            .unwrap_or(self.date.clone());

        // do the same kind of uid construction 🚧 that seemed to be happening in the legacy
        // version of the calendar creation
        let uid = {
            let prefix = "https://www.c3d2.de/#";
            format!(
                "{prefix}{text}",
                text = wrap_text(
                    prefix,
                    format!(
                        "{summary}{date}",
                        summary = urlencoding::encode(summary.clone().unwrap_or_default().as_str()),
                        date = datetime
                    )
                )
            )
        };

        // What the heck? nice format! 🤓
        fn format_datetime(s: impl AsRef<str>) -> String {
            s.as_ref().replace(":", "").replace("-", "")
        }
        let datetime_start = format_datetime(datetime);
        let datetime_end = self.end.as_ref().map(format_datetime);

        // just a stub at this point as it's not really implemented. It would probably make more
        // sense to use zolas taxonomies for this
        let categories = self.categories.as_ref().map(|cs| cs.iter().join(","));

        // 🖨️🖨️🖨️🖨️🖨️
        writeln!(f, "BEGIN:VENVENT")?;
        writeln!(f, "METHOD:PUBLISH")?;
        writeln!(f, "CLASS:PUBLIC")?;
        write_text(f, "UID", Some(uid).as_ref())?;
        write_text(f, "DTSTART", Some(datetime_start).as_ref())?;
        write_text(f, "DTEND", datetime_end.as_ref())?;
        write_text(f, "SUMMARY", summary.as_ref())?;
        write_text(f, "DURATION", self.duration.as_ref())?;
        write_text(f, "CATEGORIES", categories.as_ref())?;
        write_text(f, "URL", self.url.as_ref())?;
        write_text(f, "LOCATION", self.location.as_ref())?;
        write_text(f, "DESCRIPTION", self.description.as_ref())?;
        writeln!(f, "END:VENVENT")?;

        Ok(())
    }
}

// just prevent some repetition
fn write_text(
    f: &mut std::fmt::Formatter<'_>,
    prefix: impl AsRef<str>,
    maybe_text: Option<&impl AsRef<str>>,
) -> std::fmt::Result {
    if let Some(text) = maybe_text.map(|text| text.as_ref()) {
        let prefix = prefix.as_ref();
        let text = wrap_text(prefix, text);
        writeln!(f, "{prefix}:{text}")?;
    }
    Ok(())
}

// nice text wrapping because of recommended practices:
//
// https://www.rfc-editor.org/rfc/rfc5545#section-5
fn wrap_text(prefix: impl AsRef<str>, text: impl AsRef<str>) -> String {
    let prefix = prefix.as_ref();
    let text = text.as_ref();
    // -2 for ':' and '\n' chars
    let first_line_num_chars = OPTIMAL_LINE_WIDTH - prefix.len() - 2;
    // no need to wrap the text if it's shorter
    (first_line_num_chars < text.len())
        .then(|| {
            // the first line is a special snowflake since we need to remind ourselves that the
            // prefix also takes up some space
            let wrap = textwrap::wrap(text, first_line_num_chars);
            let first = wrap.first().map(|cow| cow.to_string()).unwrap_or_default();

            // wrap the rest of the text with optimal width
            let (_, rest) = text.split_at(first.len());
            std::iter::once(first.to_owned())
                .chain(
                    textwrap::wrap(rest.trim(), OPTIMAL_LINE_WIDTH)
                        .into_iter()
                        // 🐄 -> 📝
                        .map(|cow| cow.to_string()),
                )
                .join("\n")
        })
        .unwrap_or(text.to_owned())
}
