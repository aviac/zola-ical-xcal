{
  perSystem =
    { pkgs, ... }:
    {
      packages.zix = pkgs.rustPlatform.buildRustPackage {
        pname = "zix";
        version = "0.1.0";

        src = ./../.;

        cargoHash = "sha256-POgiTNPwrQ+n7fdvqA1YoP0gxClFCdoIh3hJzddHAL0=";

        meta.mainProgram = "zix";
      };
    };
}
