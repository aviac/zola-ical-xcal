{ inputs, ... }:
{
  perSystem =
    {
      pkgs,
      lib,
      system,
      ...
    }:
    let
      fnx = inputs.fenix.packages.${system};
      rust = fnx.combine [
        fnx.stable.cargo
        fnx.stable.clippy
        fnx.stable.rust-analyzer
        fnx.stable.rust-src
        fnx.stable.rustc

        # it's generally recommended to use nightly rustfmt
        fnx.complete.rustfmt
      ];

      generalPkgs = [
        pkgs.pkg-config
        pkgs.udev
        pkgs.alsaLib
        pkgs.vulkan-loader
        pkgs.wayland
        pkgs.libxkbcommon
        pkgs.openssl
      ];
    in
    {
      devShells = {
        rust = pkgs.mkShell {
          name = "rust shell";
          packages = generalPkgs ++ [ rust ];
          LD_LIBRARY_PATH = lib.makeLibraryPath generalPkgs;
        };
      };
    };
}
